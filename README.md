# IP ADDRESS MANAGEMENT APP
## _CAESAR IAN B._

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

Technologies used developing this app

- Linux, Nginx, MySQL & PHP
- Gitlab, Webpack, NodeJS, DDEV
- Laravel, Quasar Framework (Vue3)
- ✨Magic ✨

## Tech & Requirements

This App uses a number of open source projects to work properly:

- [DDEV](https://ddev.com/) - For Laravel docker container
- [Quasar Framework](https://quasar.dev/start/quick-start) - awesome Vue Framework components
- [node.js](https://nodejs.org/en) - evented I/O for the backend
- [Laravel](https://laravel.com/) - back-end framework
- [Webpack](https://laravel.com/) - free and open-source module bundler for JavaScript.

## Installation

Dillinger requires [Node.js](https://nodejs.org/) v10+ to run.

Install the dependencies and devDependencies and start the server.

- [DDEV](https://ddev.com/) - Refer to the official website for the full installation guide.
- [Quasar Framework](https://quasar.dev/start/quick-start) - awesome Vue Framework components
- [node.js](https://nodejs.org/en) - evented I/O for the backend


```sh
git clone git@gitlab.com:moreishi/ip-address-ms.git
```

Let's install setup laravel evironent first.

```sh
cd laravel-app
~# ddev config  
```
- complete the config setup
- select 'laravel' preconfig for the setup configuration
- make sure to use "./public" folder as webroot

```sh
~# ddev start
~# ddef launch 
```
- When configure is done run the above command
- when up and running take note on the web URL address proved from the terminal e.g http://127.0.0.1:56324 you will use it in the dotenv setup later.

## Continue
```sh
~# ddev php artisan migrate:fresh --seed
~# ddev php artisan storage:link
~# ddev php artisan key:generate
```
done! Try accessing your ddev address in your browser make sure it's start with 127.x.x.x

## Front-End

```sh
~# cd vue-quasar-project
```

dotenv

```sh
nano .env
LARAVEL_URI=http://127.0.0.1:49850 <- change this to your ddev address make sure it is 127.x.x.x
VUE_URI=http://127.0.0.1:8080 <- this is your quasar address
```

save the .env file

```sh
~# npm run dev
```
```sh
- now access your web app using http://127.0.0.1:8080 
- do not use http://localhost:8080
```

Done! 

## License

MIT

**Free Software, Hell Yeah!**
