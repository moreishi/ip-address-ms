<?php

namespace Database\Seeders;

use App\Models\Log;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $user = User::factory()->create(['name' => 'admin', 'email' => 'admin@domain.loc', 'password' => 'password123']);
        $user->logs()->save(new Log([
            'body' => "Account $user->name has been created."
        ]));
    }
}
