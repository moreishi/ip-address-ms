<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Ip;
use App\Models\Log;

class LogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $ip = Ip::all();

        foreach($ip as $item) {
            $item->logs()->save(new Log([
                'body' => "CREATED $item->ip $item->label"
            ]));
        }
    }
}
