<?php

namespace Tests\Feature\Log;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;

class LogReadTest extends TestCase
{
    
    public function test_get_logs_fail(): void
    {
        $response = $this->get('/api/logs');

        $response->assertStatus(500);
    }

    public function test_get_logs(): void
    {
        $user = User::factory()->create();

        $response = $this->actingAs($user)->get('/api/logs');

        $response->assertStatus(200);
    }
}
