<?php

namespace Tests\Feature\Auth;

use Tests\Feature\Auth\BaseAuthTest;
use App\Models\User;

class LoginTest extends BaseAuthTest
{

    public function test_post_login(): void
    {

        $user = User::factory()->create();

        $response = $this->post(self::LOGIN_URI, [
            'email' => $user->email,
            'password' => 'password'
        ]);

        $response->assertStatus(200);
    }

    public function test_post_login_invalid(): void
    {

        $user = User::factory()->make();

        $response = $this->post(self::LOGIN_URI, [
            'email' => $user->email,
            'password' => 'password123'
        ]);

        $response->assertStatus(200);
        $response->assertJsonPath('data.message', 'Unable to authenticate credentials!');
    }
}
