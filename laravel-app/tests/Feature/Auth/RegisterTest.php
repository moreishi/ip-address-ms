<?php

namespace Tests\Feature\Auth;

use Tests\Feature\Auth\BaseAuthTest;
use Database\Factories\UserFactory;
use App\Models\User;

class RegisterTest extends BaseAuthTest
{
    
    public function test_post_register(): void
    {

        $user = User::factory()->make();

        $response = $this->post(self::REGISTER_URI, [
            'name' => $user->name,
            'email' => $user->email,
            'password' => 'password123'
        ]);
        
        $response->assertStatus(200);
    }

    public function test_post_register_fail(): void
    {

        $user = User::factory()->create();

        $response = $this->post(self::REGISTER_URI, [
            'name' => $user->name,
            'email' => $user->email,
            'password' => 'password123'
        ]);
        
        $response->assertStatus(401);
    }

}
