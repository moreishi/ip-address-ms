<?php

namespace Tests\Feature\Auth;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class BaseAuthTest extends TestCase
{
    
    use RefreshDatabase;

    const LOGIN_URI = '/api/login';

    const REGISTER_URI = '/api/register';

}
