<?php

namespace Tests\Feature\IP;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Ip;

class BaseIPTest extends TestCase
{
   use RefreshDatabase;

   const IP_ADDRS_URI = '/api/ip-addresses';
}
