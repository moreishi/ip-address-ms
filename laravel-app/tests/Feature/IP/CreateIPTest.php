<?php

namespace Tests\Feature\IP;

use Tests\Feature\IP\BaseIPTest;
use App\Models\Ip;
use App\Models\User;


class CreateIPTest extends BaseIPTest
{
    public function test_create_ip_address(): void
    {
        $user = User::factory()->create();
        $ip = Ip::factory()->make();

        $response = $this->actingAs($user )->post(self::IP_ADDRS_URI, [
            'ip' => $ip->ip,
            'label' => $ip->label
        ]);

        $response->assertStatus(201);
    }

    public function test_create_ip_address_fail(): void
    {
        $user = User::factory()->create();
        $ip = Ip::factory()->make();

        $response = $this->actingAs($user )->post(self::IP_ADDRS_URI, [
            'ip' => 'not an IP Address',
            'label' => $ip->label
        ]);

        $response->assertStatus(302);
    }
}
