<?php

namespace Tests\Feature\IP;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\Feature\IP\BaseIPTest;
use App\Models\User;

class ReadIPTest extends BaseIPTest
{
    /**
     * A basic feature test example.
     */
    public function test_get_ip_addresses(): void
    {
        $user = User::factory()->create();

        $response = $this->actingAs($user)->get(self::IP_ADDRS_URI);

        $response->assertStatus(200);
    }
}
