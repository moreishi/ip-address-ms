<?php

namespace Tests\Feature\IP;

use Tests\Feature\IP\BaseIPTest;
use App\Models\User;
use App\Models\Ip;
use App\Models\Log;

class UpdateIPTest extends BaseIPTest
{
    public function test_update_label(): void
    {

        $mockIp = Ip::factory()->create();
        $mockIp2 = Ip::factory()->make();
        $user = User::factory()->create();
        
        $response = $this->actingAs($user)->put("/api/ip-addresses/$mockIp->id", [
            'ip' => $mockIp->ip,
            'label' => $mockIp2->label
        ]);
        
        $response->assertStatus(200);        
        
    }
}
