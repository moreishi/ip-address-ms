<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Response;
use App\Http\Controllers\Api\LoginController;

Route::get('/', function () {
    return view('welcome');
});



Route::get('/token', [LoginController::class, 'token']);