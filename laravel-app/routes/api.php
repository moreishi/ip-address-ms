<?php

use App\Http\Controllers\Api\LoginController;
use App\Http\Controllers\Api\RegisterController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\IpController;
use App\Http\Controllers\Api\LogController;
use App\Http\Controllers\Api\LogoutController;

Route::post('login', [LoginController::class, 'index']);
Route::post('register', [RegisterController::class, 'index']);
 
Route::middleware('auth:sanctum')->group(function() {
    Route::get('logout', [LogoutController::class, 'index']);
    Route::get('ip-addresses/{id}/logs', [IpController::class, 'logs']);
    Route::apiResource('logs', LogController::class);
    Route::apiResource('ip-addresses', IpController::class);
});