<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\DTO\UserDTO;
use App\DTO\RegisterDTO;
use App\Services\RegisterService;
use App\Http\Requests\RegisterRequest;

class RegisterController extends Controller
{

    public $registerService;

    public function __construct(RegisterService $registerService)
    {
        $this->registerService = $registerService;
    }

    public function index(RegisterRequest $request)
    {
        
        $registerDTO = new RegisterDTO(
            $request->name, 
            $request->email, 
            $request->password
        );

        return $this->registerService->register($registerDTO);
    }
}
