<?php

namespace App\Http\Controllers\Api;

use App\DTO\IpDTO;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\IpCreateRequest;
use App\Repositories\IpRepository;
use App\Services\IpService;

class IpController extends Controller
{

    public $ipRepository;
    public $ipService;

    public function __construct(IpRepository $ipRepository, IpService $ipService)
    {
        $this->ipRepository = $ipRepository;
        $this->ipService = $ipService;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return $this->ipRepository->getAll();
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(IpCreateRequest $request)
    {
        return $this->ipService->save(new IpDTO($request->ip, $request->label));
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        return $this->ipRepository->getById($id);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        return $this->ipService->update(new IpDTO($request->ip, $request->label), $id);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }

    public function logs($id) 
    {
        return $this->ipRepository->getLogsByIp($id);
    }
}
