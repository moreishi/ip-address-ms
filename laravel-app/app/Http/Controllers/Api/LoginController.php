<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\DTO\LoginDTO;
use App\Http\Resources\LoginResource;
use App\Repositories\LoginRepository;

class LoginController extends Controller
{

    public $loginRepository;

    public function __construct(LoginRepository $loginRepository)
    {
        $this->loginRepository = $loginRepository;
    }

    public function index(LoginRequest $request)
    {
        $loginDTO = new LoginDTO($request->email, $request->password);
        return $this->loginRepository->authenticate($loginDTO);
    }

    public function token() {
        return response()->json(csrf_token());
    }

}
