<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Log;


class Ip extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function logs() {
        return $this->morphMany(Log::class,'loggable');
    }

}
