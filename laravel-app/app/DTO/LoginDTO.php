<?php

namespace App\DTO;

use App\DTO\UserDTO;

class LoginDTO extends UserDTO {

    public function __construct($email, $password)
    {
        $this->email = $email;
        
        $this->password = $password;
    }

}