<?php

namespace App\DTO;

class UserDTO {

    public $name;

    public $email;

    public $password;

    public $token;

    public function __construct($name, $email, $password)
    {
        $this->name = $name;

        $this->email = $email;
        
        $this->password = $password;
    }

}