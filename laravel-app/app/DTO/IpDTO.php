<?php

namespace App\DTO;

class IpDTO {
    
    public $ip;

    public $label;

    public function __construct($ip = null, $label = null)
    {
        $this->ip = $ip;

        $this->label = $label;
    }

}