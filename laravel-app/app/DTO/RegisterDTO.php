<?php

namespace App\DTO;

use App\DTO\UserDTO;

class RegisterDTO extends UserDTO {

    public function __construct($name, $email, $password)
    {
        $this->name = $name;

        $this->email = $email;

        $this->password = $password;
    }

}