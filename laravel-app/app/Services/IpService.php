<?php

namespace App\Services;

use App\Http\Resources\IpResource;
use App\Interfaces\IIpService;
use App\DTO\IpDTO;
use App\Models\Ip;
use App\Models\Log;

class IpService implements IIpService {

    public function save(IpDTO $ipDTO) : IpResource {

        $ip = Ip::create([
            'ip' => $ipDTO->ip,
            'label' => $ipDTO->label
        ]);

        /** Logging */

        $ip->logs()->save(new Log([
            'body' => "CREATED $ipDTO->ip $ipDTO->label"
        ]));

        return new IpResource($ip);
    }

    public function update(IpDTO $ipDTO, $id) {

        $ip = Ip::find($id);
        $ip->ip = $ipDTO->ip;
        $ip->label = $ipDTO->label;
        $ip->save();

        /** Logging */

        $ip->logs()->save(new Log([
            'body' => "UPDATED $ipDTO->ip $ipDTO->label"
        ]));

        return new IpResource($ip);
    }

}