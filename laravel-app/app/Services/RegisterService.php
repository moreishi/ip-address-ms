<?php

namespace App\Services;

use App\DTO\RegisterDTO;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Http\Resources\RegisterResource;
use App\Models\Log;

class RegisterService {

    public function register(RegisterDTO $registerDTO) {

        $user = new User();
        $user->name = $registerDTO->name;
        $user->email = $registerDTO->email;
        $user->password = Hash::make($registerDTO->password);
        $user->save();

        $user->logs()->save(new Log([
            'body' => "Account $user->name has been created"
        ]));

        $token = $user->createToken('auth_token')->plainTextToken;

        return new RegisterResource([
            'name' => $user->name,
            'email' => $user->email,
            'token' => $token
        ]);
    }

}