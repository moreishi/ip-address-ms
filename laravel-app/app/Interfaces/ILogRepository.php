<?php

namespace App\Interfaces;

use App\Http\Resources\LogResource;

interface ILogRepository {

    public function getAll() : LogResource;

    public function getById($id) : LogResource;

}