<?php

namespace App\Interfaces;

use App\Http\Resources\IpResource;
use App\DTO\IpDTO;

interface IIpService {

    public function save(IpDTO $ipDTO) : IpResource;

}