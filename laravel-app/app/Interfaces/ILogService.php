<?php

namespace App\Interfaces;

use App\Http\Resources\LogResource;

interface ILogService {

    public function getAll() : LogResource;

    public function getById() : LogResource;

}