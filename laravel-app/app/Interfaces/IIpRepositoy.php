<?php

namespace App\Interface;

use App\Http\Resources\IpResource;

interface IIpRepository {

    public function getAll() : IpResource;

    public function getById() : IpResource;

}