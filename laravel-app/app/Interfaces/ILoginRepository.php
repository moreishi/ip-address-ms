<?php

namespace App\Interfaces;

use App\Http\Resources\LoginResource;

interface ILoginRepository {
    
    public function authenticate() : LoginResource;

}