<?php

namespace App\Repositories;

use App\Interfaces\ILogRepository;
use App\Http\Resources\LogResource;
use App\Models\Log;

class LogRepository implements ILogRepository{

    public function getAll() : LogResource 
    {
        return new LogResource(Log::orderBy('id','desc')->get());
    }

    public function getById($id) : LogResource
    {
        return new LogResource(Log::where('id', $id)->orderBy('id','desc')->get());
    }

}