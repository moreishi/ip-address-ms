<?php

namespace App\Repositories;

use App\Http\Resources\IpResource;
use App\Interface\IIpRepository;
use App\Models\Ip;

class IpRepository {

    public function getAll() : IpResource
    {
        return new IpResource(Ip::orderBy('created_at','desc')->get());
    }

    public function getById($id) : IpResource
    {
        return new IpResource(Ip::find($id));
    }

    public function getLogsByIp($id)
    {
        $ip = Ip::where('id', $id)->with('logs')->first();
        return new IpResource($ip);
    }

}