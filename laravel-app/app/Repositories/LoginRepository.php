<?php

namespace App\Repositories;

use App\Http\Resources\LoginResource;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use App\DTO\LoginDTO;
use App\DTO\UserDTO;
use App\Models\Log;

class LoginRepository {


    public function authenticate(LoginDTO $loginDTO) : LoginResource {

        if(Auth::attempt(['email' => $loginDTO->email, 'password' => $loginDTO->password])) {
            
            $token = request()->user()->createToken('auth_token')->plainTextToken;
            
            $user = User::find(Auth::user()->id);
            $user->logs()->save(new Log([
                'body' => "SYSTEM new login session from $user->name"
            ]));

            return new LoginResource([
                'name' => Auth::user()->name,
                'email' => Auth::user()->email,
                'token' => $token
            ]);
        }

        return new LoginResource([
            'message' => 'Unable to authenticate credentials!'
        ]);

    }

}