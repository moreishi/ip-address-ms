import { useUserStore } from 'src/stores/user-store';
import { computed } from 'vue';

export default function (to, from, next) {
  const userStore = useUserStore();
  const user = computed(() => userStore.getUser);

  if (to.name == 'login' && user.value.hasOwnProperty('token')) {
    return next({ name: 'home' });
  }

  if (to.name != 'login' && !user.value.hasOwnProperty('token')) {
    return next({ name: 'login' });
  }

  return next();
}
