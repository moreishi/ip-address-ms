import { defineStore } from 'pinia';
import { api } from 'boot/axios';

// export type TUser = {
//   name: string;
//   email: string;
//   token: string;
// };

// export interface IState {
//   user: TUser;
// }

// export type TAuth = {
//   email: string;
//   password: string;
// };

export const useLogStore = defineStore('log', {
  state: () => ({
    logs: {},
  }),
  getters: {
    getLogs: (state) => state.logs,
  },
  actions: {
    async fetchLogs() {
      const { data } = await api.get('/api/logs');
      this.logs = [...data.data];
    },
    async fetchLogsById(id: number) {
      const { data } = await api.get(`/api/ip-addresses/${id}/logs`);
      this.logs = [...data.data.logs];
    },
  },
});
