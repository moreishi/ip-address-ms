import { defineStore } from 'pinia';
import { api } from 'src/boot/axios';
// import { useQuasar } from 'quasar';

// const $q = useQuasar();

export type TIPAddress = {
  id: number | null;
  ip: string | null;
  label: string | null;
};

export interface IState {
  ipAddresses: TIPAddress[];
  ipAddress: TIPAddress;
}

export const IP_ADDR_URI = '/api/ip-addresses';

export const useIPAddressStore = defineStore('ipAddress', {
  state: () =>
    <IState>{
      ipAddresses: [],
      ipAddress: {
        id: null,
        ip: null,
        label: null,
      },
    },
  getters: {
    getIpAddresses: (state) => state.ipAddresses,
    getIpAddress: (state) => state.ipAddress,
  },
  actions: {
    async fetchIpAddresses() {
      const { data } = await api.get(IP_ADDR_URI);
      this.ipAddresses = [...this.ipAddresses, ...data.data];
    },
    async storeIpAddress(ip: TIPAddress) {
      const res = await api.post(IP_ADDR_URI, ip);
      const { data } = res;
      this.ipAddresses = [data.data, ...this.ipAddresses];
      return res;

      // $q.notify({
      //   message: 'Invalid IP Address',
      //   color: 'red',
      // });
    },
    setIpAddress(ip: TIPAddress) {
      this.ipAddress = ip;
    },
    async updateIp(ip: TIPAddress) {
      await api.put(IP_ADDR_URI + '/' + ip.id, ip);
    },
  },
});
