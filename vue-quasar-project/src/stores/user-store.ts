import { defineStore } from 'pinia';
import { api } from 'boot/axios';

export type TUser = {
  name: string;
  email: string;
  token: string;
};

export interface IState {
  user: TUser;
}

export type TAuth = {
  email: string;
  password: string;
};

export const useUserStore = defineStore('user', {
  state: () => ({
    user: {},
  }),
  getters: {
    getUser: (state) => state.user,
  },
  actions: {
    async initToken() {
      await api.get('/sanctum/csrf-cookie');
    },
    initUser() {
      const user: string = localStorage.getItem('user') || '';
      if (user == 'null') return;
      const userObject: TUser = JSON.parse(user);

      api.defaults.headers.common['Authorization'] =
        'Bearer ' + userObject.token;

      this.user = userObject;
    },
    async fetchUser(payload: TAuth) {
      const { data: user } = await api.post('/api/login', payload);
      api.defaults.headers.common['Authorization'] =
        'Bearer ' + user.data.token;
      this.user = user.data;
      localStorage.setItem('user', JSON.stringify(this.user));
    },
    async logout() {
      this.user = {};
      localStorage.setItem('user', JSON.stringify({}));
    },
  },
});
